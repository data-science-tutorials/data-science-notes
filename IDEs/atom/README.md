# [Atom](https://atom.io/)

[TOC]

---
# Sources/Reading
- [atom](https://atom.io/)


---
# Installation
- Details for Debian but always double check official installation instructions [here](https://flight-manual.atom.io/getting-started/sections/installing-atom/).
```bash
wget -qO - https://packagecloud.io/AtomEditor/atom/gpgkey | sudo apt-key add -
sudo echo "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main" > /etc/apt/sources.list.d/atom.list
sudo apt update && apt install atom -y
```

---
# Global Configuration
- save as favourite icon on dash/dock
- preferences > core
	- Close Deleted File Tabs
	- Restore Previous Windows on Start NO
	- Do not send telemery data
	- Warn on files larger that 5MB
- preferences > editor
	- enable indent on new line
	- disable auto-indent on paste
    - font size 16
	- scroll past end
	- show indent guide
	- show invisibles
	- tab length 4
- preferences > theme
	- Syntax Theme: Solarised Dark
- preferences > packages
	- Spell Check > Locales add `en-GB, es-ES`
		- You need to have already installed these languages on your system
	- tree-view
		- "Settings" > Uncheck "Hide VCS Ignored Files". This is just a recommendation, but otherwise you won't see the folders and files included in .gitignore.
- preferences > Install
	- file-icons
	- minimap
	- linter
	- minimap-linter
	- minimap-selection
	- minimap-find-and-replace
	- minimap-highlight-selected
	- highlight-selected
	- pigments
	- terminus
		- close terminal on exit
	- autocomplete-project-paths
	- rainbow-csv
	- markdown-preview-enhanced
		- setup you preferred theme
	- zentabs
- Reboot atom, it may ask to install missing dependencies, accept.


---
# Specific Programming Languages Configuration
- Go to each programming language directory and check for additional info about atom.

## R
- atom-language-r
- autocomplete-R
- lintr
	- first on a root terminal `sudo apt install libxml2-dev libssl-dev libcurl4-openssl-dev -y`
	- on a non-admin terminal
		- `R`
		- `install.packages("lintr")`
	- on atom install: linter-lintr

## python
- [ide-python](https://atom.io/packages/ide-python)
	- Installation
```bash
pip3 install 'python-language-server[all]'
apm install atom-ide-base
apm install ide-python
```
- Enable (disable) what you want (don't want).
- [docblock-python](https://atom.io/packages/docblock-python)
- Virtualenv
	- (terminal) `pip3 install virtualenv`
	- atom-python-virtualenv
	- go to settings of the package and add the paths to your projects


## SQL
- autocomplete-sql

## docker
- docker
- language-docker
- linter-docker

## yaml
- `pip3 install yamllint`
- linter-yaml

## git
- git-time-machine
- minimap-git-diff
- (experimental) git-control-fork

## bash
- terminal `sudo apt install shellcheck -y`
- on atom: linter-shellcheck

## powershell
- language-powershell

## ini
- language-ini-desktop


---
# LaTeX on atom
- Install LaTeX `sudo apt install texlive-full`
- atom-latex
	- disable build on save
	- enable save before build
	- focus PDF after build
	- view PDF in viewer tab
	- add `*synctex.gz`, to the list of files to clean
	- cleaning of aux files


---
# Run code interactively - Hydrogen

## Jupyter Kernels

- **R**: Install a jupyter kernel (see [this](../R/jupyter-kernels/README.md) page for more details).
- **python**: Install a jupyter kernel (see [this](../python/jupyter-kernels/README.md) page for more details).
- Remember to register additional jupyter kenerls for each python (and R) project that uses virtualenv (or renv).


## Atom's package: Hydrogen

- Back on atom > install
	- hydrogen
		- settings
		- view output in the dock
		- enable global kernel
		- enable kernel notification
	- hydrogen-exectime
	- data-explorer
	- hydrogen-python

## Keybindings
- Edit > Preferences > Keybindings
- Search for "hydrogen" and click on the copy symbol on the left of the line with the "hydrogen:run-all" command
- Click on "your keymap file"
- Paste your clipboard an change the second line for `  'ctrl-alt-b': 'hydrogen:run-all-above'`

## Connect to a remote kernel
- Edit > Preferences > Packages > Hydrogen > Kernel Gateways
- Copy-paste JSON syntax with the remote kernel details, e.g.,
```json
[{
  "name": "remote-kernel-name-1",
  "options": {
    "baseUrl": "http://127.0.0.1:8888",
    "token": "bb542a4c0f2430c7904f452b7685e3883c4606bd360179bc"
  }
},
{
  "name": "remote-kernel-name-2",
  "options": {
    "baseUrl": "http://127.0.0.1:8889",
    "token": "da37364b020f8b5a261bf23ec9fafbd978fc3f6631444887"
  }
}]
```
- Then, Packages > Hydrogen > Connect to Remote Kernel and the names of your kernels should pop up (in the example, "remote-kernel-name-1" and "remote-kernel-name-2").

- _Note_: The included JSON corresponds to two Jupyter kernels. In such a case, the port and the token are generated each time you launch the server unless they are specified. However, under some situations, you may want them to take some particulra values. To force the values of the variables, see _jupyter_server/jupyter_config.md_.
