# [VSCodium](https://github.com/VSCodium/vscodium)

[TOC]

---
# Sources/Reading
- [VSCodium](https://github.com/VSCodium/vscodium)

---
# Extensions
- Python
- autoDocstring
- Jupyter
- Jupyter keymap
- Jupyter Notebbok Renderers


---
# Configuration

## Global
- Set up your preferred theme.
- Font size.
- Tab Size.
- Workbench > Editor: Restore View State: `disabled`
- Window: Restore Windows: `none`
- Terminal: Integrated: Enable Multi Line Paste Warning: `disabled`

## Python

### Global
- defaultInterpreterPath: `python3`

### Linting
- [Flake8](https://github.com/pycqa/flake8)
- `pip3 install flake8`
- Python > Linting: Flak8 Enabled
- Python > Linting: Flake8 Path: `flake8`
- Python > Linting: Flake8 Arga
```
--max-line-length
--ignore="W503"
```

### Language Server
- [Jedi](https://github.com/davidhalter/jedi)
- `pip3 install jedi`
- Python > Language Server > `Jedi`

### Formatting
- [Black](https://github.com/psf/black)
- `pip3 install black`
- Python > Formatting > Black Path: `black`
- Python > Formatting > Provider: `black`
- Python > Formatting: Black Args
```
--line-length
119
```
- Editor: Format On Save: `enabled`

## Git
- Git: Confirm Sync: `disabled`
- Git: AutoFetch : `true`
- Git: Merge Editor: `enabled`

## Jupyter
- Jupyter: Ask For Kernel Restart: `disabled`
- Notebook: Line Numbers: `on`
- Jupyter: Notebook Root Folder: ${workspaceFolder} (in this way, the root folder to run code is always the folder where you run the IDE). __WARNING__: Tested just in VS Code.
- Jupyter: Send Selection To Interactive Window: Check the option (in this way, when you run individual lines of code, they go to the interactive windows instead of the terminal; of course, do not do it if you prefer to run the code in the terminal). __WARNING__: Tested just in VS Code.

## Open folder with the IDE

### Linux

__WARNING__: This has not been tested and is supposed to work with Ubuntu 20.04 + VS Code.

The bash script with the orders to add this functionality is in https://raw.githubusercontent.com/cra0zy/code-nautilus/master/install.sh. We recommend to revise the content of the script before doing anything. If the content is correct (you should be really sure about this), run the following command.
```bash
wget -qO- https://raw.githubusercontent.com/cra0zy/code-nautilus/master/install.sh | bash
```
If you have to modify the content because it contains malicious lines or for whatever reason, download the script, modify it, and run manually.

Source: https://stackoverflow.com/questions/66483057/how-to-add-open-with-code-with-right-click-in-ubuntu-or-pop-os

### Windows

__WARNING__: Tested just in VS Code.

You have to:
1. Create a file with .reg extension (let us call it vsCodeOpenFolder.reg).
2. Add the following text to the file vsCodeOpenFolder.reg (remember to modify properly the path with the .exe file in the file; besides, notice once again that the text corresponds to VS Code and you will have to replace Code by Codium for VS Codium):
```
Windows Registry Editor Version 5.00
; Open files
[HKEY_CLASSES_ROOT\*\shell\Open with VS Code]
@="Edit with VS Code"
"Icon"="C:\\Program Files\\Microsoft VS Code\\Code.exe,0"
[HKEY_CLASSES_ROOT\*\shell\Open with VS Code\command]
@="\"C:\\Program Files\\Microsoft VS Code\\Code.exe\" \"%1\""
; This will make it appear when you right click ON a folder
; The "Icon" line can be removed if you don't want the icon to appear
[HKEY_CLASSES_ROOT\Directory\shell\vscode]
@="Open Folder as VS Code Project"
"Icon"="\"C:\\Program Files\\Microsoft VS Code\\Code.exe\",0"
[HKEY_CLASSES_ROOT\Directory\shell\vscode\command]
@="\"C:\\Program Files\\Microsoft VS Code\\Code.exe\" \"%1\""
; This will make it appear when you right click INSIDE a folder
; The "Icon" line can be removed if you don't want the icon to appear
[HKEY_CLASSES_ROOT\Directory\Background\shell\vscode]
@="Open Folder as VS Code Project"
"Icon"="\"C:\\Program Files\\Microsoft VS Code\\Code.exe\",0"
[HKEY_CLASSES_ROOT\Directory\Background\shell\vscode\command]
@="\"C:\\Program Files\\Microsoft VS Code\\Code.exe\" \"%V\""
```
3. Double click on the file and accept.

If you right click now on a folder or in the middle of the file explorer, you should see an option to "Open Folder as VS Code Project".

Source: https://thisdavej.com/right-click-on-windows-folder-and-open-with-visual-studio-code/
