# SSH

[TOC]

# Generating keys

To generate a pair of public-private ssh keys:

```bash
ssh-keygen -t rsa
```

Additionally, you can choose a passphrase.

# git clone remote repositories with ssh protocol

First of all, open `~/.ssh/id_rsa.pub` with your favorite text editor. This file contains the public key you just created. Copy the key, including the first part, starting with ssh-rsa.

Then, go to the host where you want to clone some repositories from.
* If it is GitHub, click on your profile avatar (upper-right corner) > Settings on the drop-down menu > SSH and GPG keys (left-bar menu) > New SSH keys > Paste the ssh public key you created into "Key" box (optionally, you can choose a title) > Add SSH key.
* If it is GitLab, click on your profile avatar (upper-right corner) > Preferences on the drop-down menu > SSH Keys (left-bar menu) > Paste the ssh public key you created into "Key" box (optionally, you can choose a title and an expiration date) > Add key.
