# Data Science Notes

[TOC]

---
# Contributors
- Jorge Pérez Heredia
- Eduardo Sánchez Burillo

---
# Summary
This repository contains notes on different aspects related to data science. In every folder there are markdown files with some essentials, and/or minimal examples.
