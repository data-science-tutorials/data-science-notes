# Python

[TOC]

# Sources/Reading
- [Project Structure and Imports](https://dev.to/codemouse92/dead-simple-python-project-structure-and-imports-38c6)
- [How To Package Your Python Code](https://python-packaging.readthedocs.io/en/latest/)
