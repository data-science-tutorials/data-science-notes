# Python

[TOC]

# Sources/Reading
- [Python](https://www.python.org/)
- [Kaggle's Python Tutorial](https://www.kaggle.com/learn/python)
- [Dictionary pandas-dplyr](https://gist.github.com/conormm/fd8b1980c28dd21cfaf6975c86c74d07)


# Installation
- Already installed on GNU/Linux


# Install python packages
- Install pip3 `sudo apt install python3-pip -y`
- Now just run `pip3 install [pkg-name]`
