# Python Jupyter Kernels

[TOC]

# Sources/Reading
- [Python](https://www.python.org/)
- [Jupyter](https://jupyter.org/)


# Register a global kernel for python
```bash
pip3 install ipykernel
python3 -m ipykernel install --user
```

# Register a specific kernel
```bash
python3 -m ipykernel install --user --name=[python-kernel-name]
```

# Deleting a kernel
- Just remove the corresponding files on `/home/[USER_NAME]/.local/share/jupyter/kernels/`
