# Virtualenv - Virtualisation on Python

[TOC]

# Sources/Reading
- [Virtualenv](https://virtualenv.pypa.io/en/stable/)
- [Pipenv vs virtualenv vs conda environment](https://medium.com/@krishnaregmi/pipenv-vs-virtualenv-vs-conda-environment-3dde3f6869ed)


# Installation
- `pip3 install virtualenv`


# Start a project from zero with virtualenv
- Go to the project's directory
- Create the virtual env `virtualenv [venv-my-project]`
- Initialise it `source venv-my-project/bin/activate`. You will get feedback on your console prompt.
- Install packages normally with pip `pip3 install pandas`
- Export a snapshot of the virtual environment `pip3 freeze > requirements.txt`
- Deactivate the virtual environment `deactivate`


# Recreate the venv elsewhere
- Go to the project's directory
- `pip3 install -r requirements.txt`
- When you install something which is not in the pypi repositories, you have to specify the URL with the argument -f:
```
pip install mypackage -f url_of_mypackage
```
In such a case, to recreate the virtual environment you should:
```
pip install -r requirements.txt -f url_of_mypackage
```
In case you used two or more packages in your requirements.txt with different url's, I guess you have to concatenate the url's like this:
```
pip install -r requirements.txt -f url_of_mypackage_1 url_of_mypackage_2, ...
```

# Using jupyter? VIRTUAL_ENV + JUPYTER
- Create a kernel for just the virtualenv
- Check the file *python/python-jupyter-kernels.md*
