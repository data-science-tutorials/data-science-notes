# renv - virtualisation with R

[TOC]

# Reading
- [renv](https://rstudio.github.io/renv/)
- [packrat](https://rstudio.github.io/packrat/)


# Installation
- Launch R and simply run `install.packages("renv")`


# Start a project with renv from zero
- From your project's root directory, start R
- run `renv::init()`
- From now on, while inside this project, the R-command `.libPaths()` should only point inside your project's directory.
- Install libraries as always `install.packages("ggplot2")`
- Create a snapshot of the project's libraries and their versions `renv::snapshot()`. This produces a file named (by default) *renv.lock*


# Add an existing project to renv
- From your project's root directory, start R
- run `renv::init()`, renv should profile your code and figure out which libraries are used within.
- Create a snapshot of the project's libraries and their versions `renv::snapshot()`. This updates *renv.lock*.

**WARNING**: If you installed a package (no matter if it was via `install.package()` or `devtools::install_gitlab/github/etc.()`), if the package is not used in the source code of the project (explicitly, using some of its functions, or as a dependency of other package), it shall not be included in *renv.lock*.


# Recreate a project with renv
- From your project's root directory, start R
- Run `renv::restore()`, this reads the *renv.lock* and install all the requirements.

## Recreate a project with packages from private repositories
Let us assume you want to install a package from a private repository. E.g., from a GitLab repository named *some_repository* and you have been granted with some access token with value *token*. In such a case, you install the package with `devtools::install_gitlab(repo = "some_repository", auth_token = "token")` (install devtools first if needed). When you execute `renv::snapshot()` afterwards, all the information of the installation but the autorization token is registered in *renv.lock*. Then, if you try to recreate the environment with `renv::restore()`, the execution fails because it does not find the token. You have to execute the following commands previously in your R session:
- `Sys.getenv("GITLAB_PAT")`. The output should be empty. Otherwise, be careful.
- `Sys.setenv(GITLAB_PAT = "token")`. This sets the environment variable GITLAB_PAT (GitLab Personal Access Token) as the token for the repository you want to install the package from. If `auth_token` is not specified during the installation, R looks at this variable.
- `renv::restore()`.

**WARNING**: If you want to install more than one packages from the same host (e.g., GitLab), this strategy is not valid.

## Extensions to other host services: GitHub and Bitbucket

We explain here how to do it for GitHub and Bitbucket.

**DISCLAIMER**: This has not been tested.

* For **GitHub**, you have to follow the very same procedure up to a couple of minor modifications:
    * To install the package, execute  `devtools::install_github(repo = "some_repository", auth_token = "token")`
    * To recreate the package, before `renv::restore()`, execute `Sys.setenv(GITHUB_PAT = "token")`.
* For **Bitbucket**:
    * To install the package, execute  `devtools::install_bitbucket(repo = "some_repository", auth_user = "your_bitbucket_user", password = "your_bitbucket_password")`. Notice that here a token is not used, but access must have been previously granted to your user.
    * To recreate the package, before `renv::restore()`, execute `Sys.setenv(BITBUCKET_USER = "your_bitbucket_user")` and `Sys.setenv(BITBUCKET_PASSWORD = "your_bitbucket_password")`.

---
# Using jupyter or hydrogen-based IDEs?
- Create a kernel for the renv-project
- Check the file *R/R-jupyter-kernels.md*

---
# Issues

## Connectivity (renv + Windows Server)

By default, renv uses `curl` command to download files and communicate with remote webs. However, this can be an issue if your machine has some connectivity limitations. E.g., it typically occurs on Windows machines in enterprise environments. Actually, under this situation, it may be the case that you cannot install packages in a renv environment, but you can do it without renv.

If so, execute:
```R
renv:::renv_download_file_method()
```
The output will be `curl` most likely. Then, execute:
```R
getOption("download.file.method")
```
This will be the default download method used by R. In a Windows machine, it will be `wininet` most likely. Anyway, to force renv to use the default method to download files for R, run:
```
Sys.setenv(RENV_DOWNLOAD_FILE_METHOD = getOption("download.file.method"))
```
and check whether the problem remains or not. If it remains, you can try to explicitly choose a method:
```
# Windows' internal download machinery
Sys.setenv(RENV_DOWNLOAD_FILE_METHOD = "wininet")
# R's bundled libcurl implementation
Sys.setenv(RENV_DOWNLOAD_FILE_METHOD = "libcurl")
```
Once you have found a solution (let's assume `getOption("download.file.method")` worked), you can add `Sys.setenv(RENV_DOWNLOAD_FILE_METHOD = getOption("download.file.method"))` to your .Rprofile or add the following line to your .Renviron file:
```
RENV_DOWNLOAD_FILE_METHOD = getOption("download.file.method")
```

If the problem persists, you may consider moving to packrat.

**Source**: https://github.com/rstudio/renv/blob/master/vignettes/renv.Rmd
