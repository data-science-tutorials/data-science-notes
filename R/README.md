# R

[TOC]


---
# Reading/Sources
- [R](https://cran.r-project.org/)
- [Tutorial for Beginners](https://data-flair.training/blogs/r-tutorials-home/)
- [R Studio Cheatsheets](https://rstudio.com/resources/cheatsheets/)
- [ggplot2](https://ggplot2.tidyverse.org/)


---
# Installation

Follow these instructions to install the last stable version for Debian-based distributions (tested in Ubuntu and Linux Mint). If you want to install the most recent version, go to the next subsection.

- install R `apt install r-base r-base-dev -y`
- We don't want garbage in root, so
```bash
su - root
echo "alias R='(R --vanilla)'" >> /root/.bash_aliases
. .bashrc
exit
```
- I also find annoying having the personal libraries in my home directory at plain sight. To hide this:
	- get the R-version and your platform details with the command `R --version`
	- `sudo mkdir -p /root/config-backups/etc/R && cp /etc/R/Renviron /root/config-backups/etc/R/Renviron.$(date +%Y-%m-%d)`
	- `sudo nano /etc/R/Renviron`
	- change the line:
		- `R_LIBS_USER=${R_LIBS_USER-'~/R/${platform}/${R-version'}`
		- to `R_LIBS_USER=${R_LIBS_USER-'~/.R/${platform}/${R-version'}`

## Installing most recent version

In order to install the most recent version in Ubuntu-based distributions (tested in Linux Mint 20), run as root or with sudo:

- `apt update -qq`
- `apt install --no-install-recommends software-properties-common dirmngr`
- `apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9`
- `add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"`
	- ___Note 1___: This line is valid for Ubuntu. If your are using Linux Mint, make sure first on the version of Ubuntu your Linux Mint is based on. To do so, visit first https://www.linuxmint.com/download_all.php and check. It may be Focal, Bionic, etc. Then, replace `$(lsb_release -cs)` by `focal`, `bionic`, etc.
	- ___Note 2___: After your execute this line, `deb https://cloud.r-project.org/bin/linux/ubuntu focal-cran40/` (if your Ubuntu version is Focal) should be added to /etc/apt/sources.list.d/additional-repositories.list.
	- ___Note 3___: This line adds the repository to install R 4 to your system. For future releases, modify properly. E.g. most likely for R 5 is hould be something like `add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran50/"`
- `apt-get update`
- `apt install --no-install-recommends r-base`

Sources:
* https://cloud.r-project.org/bin/linux/ubuntu/
* https://cran.r-project.org/bin/linux/ubuntu/fullREADME.html

## Installing Libraries
- Launch R by running the command `R` on a terminal
- run the command `install.packages("pkg-name")`, where `pkg-name` is the name of the packages that you want to install.
- For version control, use renv. See R-renv.md in this directory.
