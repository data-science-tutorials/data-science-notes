# R Jupyter Kernels

[TOC]

---
# Sources/Reading
- [R](https://cran.r-project.org/)
- [Jupyter](https://jupyter.org/)


---
# Instalation

## Install Jupyter
- `sudo apt install jupyter-client`


## Install a global kernel for R
- Launch an R console
```R
install.packages('repr')
install.packages('IRdisplay')
install.packages('evaluate')
install.packages('crayon')
install.packages('pbdZMQ')
install.packages('uuid')
install.packages('digest')
install.packages('IRkernel')
IRkernel::installspec()
```
- probably some of these packages installations have failed, check the error message for missing libraries like `libgit2-dev`, `libssh2-1-dev` and install them with `apt install`


## Install a specific R kernel
```R
install.packages("IRkernel")
IRkernel::installspec(name = "project-name", displayname="R_project-name")
```

---
# Deleting a kernel
- Just remove the corresponding files on `~/.local/share/jupyter/kernels/`
