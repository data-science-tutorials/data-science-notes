# How to send emails from R

[TOC]

---
# Requirements
- R
- blastula `install.packages("blastula")`
- An account on a smtp email server (e.g., Outlook)

---
## Minimum example
- Create an .R file with the following code:
```R
library(blastula)
email <- compose_email(body = md(c("<html>Test <strong>email</strong> body</html>")),
                        footer = md(c("Test email footer", Sys.Date(), ".")))
smtp_send(email = email,
          from = "user@sender.com",
          to = "user@recipient.com",
          credentials = creds(host = "smtp.mailtrap.io",
                              port = 25,
                              user = "********"))
```
- Edit accordingly and run.
- You will be prompt for the password

---
# Reading credentials from a file
- You can also create a credentials file with this command:
```R
create_smtp_creds_key(id = "mailtrap",
                      host = "smtp.mailtrap.io",
                      port = 25,
                      user = "********")
```
- And change the *credebtials* argument of the *smtp_send* function to use file:
```R
smtp_send(email = email,
          from = "user@sender.com",
          to = "user@recipient.com",
          credentials = creds_key("mailtrap"))
```
