# GitLab

[TOC]

# Sources/Reading
- [GitLab](https://gitlab.com/)


# User Tokens
- Go to gitlab.com
- Click on your avatar and "Settings"
- Click "Access Tokens" in the left panel
- Name: Whatever you want
- Expires at: Whatever you want (included nothing)
- Scopes: Whatever you want
- Create personal access token
- Copy "your new personal access token" <private token>
- Open git bash
- cd to dir where you want to clone
- git clone https://<gitlab-ci-token>:<private token>@git.example.com/myuser/myrepo.git
In our case: @gitlab.com/predictland/bsh/bsh_certificados-pdf.git
- to delete the token, go to settings > Access Tokens > Go to the token and click on Revoke


# Repository Tokens
- Go to gitlab.com
- Go to the repo
- Left panel > Settings > Repository > Deploy tokens
- Name: Whatever you want
- Expires at: Whatever you want (included nothing)
- Username: whatever you want
- Scopes: Whatever you want
- Create deploy token
- Save the login credential
- Open git bash
- cd to dir where you want to clone
- git clone https://<username>:<deploy token>@git.example.com/myuser/myrepo.git
In our case: @gitlab.com/predictland/bsh/bsh_certificados-pdf.git
- to delete the token, Left panel > Settings > Repository > Deploy tokens > Revoke
