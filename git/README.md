# git

[TOC]

# Sources/Reading
- [git](https://git-scm.com/)
- [Documentation](https://git-scm.com/book/en/v2)
- [Video Tutorials](https://www.gitkraken.com/resources/learn-git)
- [git-flow](https://github.com/nvie/gitflow)
- [How to use git flow](https://jeffkreeftmeijer.com/git-flow/)


# Installation
- Installation `sudo apt update && sudo install git -y`

# Configuration
- Configure
```bash
git config --global user.email "your.email@address.com"
git config --global user.name "Name Surename"
```
- prepare dir
```bash
mkdir ${HOME}/Repositories
cd ${HOME}/Repositories
```
- clone the repos
	- log-in your git provider (gitlab, github, ...)
	- add the public part of your ssh-key
	- `cd ${HOME}/Repositories`
	- clone the repos `git clone [repos-ssh-url]`


# Several Profiles on the same device
- Example for a work and a personal profile.
- `nano ${HOME}/.ssh/config` and add the lines
```
Host gitlab-personal
	HostName gitlab.com
	IdentityFile ""/path/to/your/personal/github/private/key"
	User user
```
```
Host gitlab-work
	HostName gitlab.com
	IdentityFile ""/path/to/your/work/github/private/key"
	User workuser
```
- As normal user
	- `git clone git@gitlab-personal:company/project.git`
- As workuser
	- add the ssh key `ssh-add ~/.ssh/id_rsa_work_user1`
	- `git clone git@gitlab-work:company/project.git`


# [meld](https://meldmerge.org/)
- `sudo apt install meld -y`
- meld > preferences > Syntax highlighting color scheme > Meld dark scheme


# [git-flow](https://github.com/nvie/gitflow)
- `sudo apt install git-flow -y`
- [How to use git flow](https://jeffkreeftmeijer.com/git-flow/)


# Display branch info on the bash prompt
- Open your *.bashrc* file, `nano ~/.bashrc`
- Replace the value of the variable PS1 on the `if [ "$color_prompt" = yes ];` for the following ` PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[01;31m\]$(__git_ps1)\[\033[00m\] \$ '`.
- Finally reload, `. .bashrc`.


# Script to Update all Repos
```bash
mkdir ${HOME}/bin
cp ${path-to-this-repo}/git/update-repos.sh ${HOME}/bin/
mv ${HOME}/bin/update-repos.sh ${HOME}/bin/update-repos
chmod +x ${HOME}/bin/update-repos
```

# Pushing to multiple hosts
- Typically you have a remote called "*origin*" with a URL (e.g. *url-origin.git*), we want to specify for this remote, 2 different URLs:
	- The original one: `git remote set-url --add --push origin url-origin.git`
	- The secondary one: `git remote set-url --add --push origin new-url.git`
- Now after committing any changes, you just have to push to all URLs this way `git push all [branch-name]`.
