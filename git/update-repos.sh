#!/bin/bash

#I'll asumme that you have all your repos in the following dir
dir_repos="${HOME}/Repositories"

#go through all the directories in ${dir_repos}
for loop_repo in "${dir_repos}"/*; do

    # handle the case of nothing in ${dir_repos}
    [[ -e "${loop_repo}" ]] || break

    #print status
    echo -e "\n Updating repository ${loop_repo}..."

    #check if its svn or git and update
    if [[ -d "${loop_repo}/.svn"  ]]; then
        (cd "${loop_repo}" && svn update)
    elif [[ -d "${loop_repo}/.git" ]]; then
        (cd "${loop_repo}" && git pull)
    fi

done
