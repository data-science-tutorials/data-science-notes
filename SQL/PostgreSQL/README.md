# PostgreSQL SQL Server

[TOC]

---
# Sources/Reading
- [PostgreSQL](https://www.postgresql.org/)
- https://www.microfocus.com/documentation/idol/IDOL_12_0/MediaServer/Guides/html/English/Content/Getting_Started/Configure/_TRN_Set_up_PostgreSQL.htm
- https://www.postgresql.org/docs/8.0/sql-createuser.html
- https://www.postgresql.org/docs/9.0/sql-grant.html

---
# Installation

## Install PostgreSQL on Linux/MacOS

TO COMPLETE

## Install PostgreSQL on Windows 10

* Go to https://www.enterprisedb.com/downloads/postgres-postgresql-downloads and download the installer for your OS (this is valid for any OS, but for Linux or MacOS you should use your terminal).
* Execute the file and accept the default options.
* The wizard proposes to install Stack Builder. Accept.
* In drivers, choose ODBC.

To interact with PostgreSQL, we use SQL Shell (psql):

* Open SQL Shell (psql)
* It will ask for several parameters: Server, Database, Port, and Username; leave them as default.


---
# Usage

## Creating a database

* Run the following command on psql (on Linux or MacOS, probably you have to run first `sudo psql` on your terminal; on Windows, open psql):

`CREATE DATABASE [db_name] WITH ENCODING '[my_encoding]' LC_COLLATE='' LC_CTYPE='';`

_Note_: Choose LC_COLLATE and LC_CTYPE according to your needs.

## Creating a user

* Run:

`CREATE USER [user_name] WITH PASSWORD '[password]';`

You can create a user without password with:

`CREATE USER [user_name];`

## Granting privileges

* Run:

`GRANT ALL PRIVILEGES ON DATABASE [db_name] TO [user_name];`

REMARK: You may not want to grant all privileges. If so, see [3].

## User interface

You can use DBeaver.

## Miscellanea

* Column name are in lower case. If you run this sql script to create a table:

```sql
create table my_schema.my_table(
    COL1 bigint;
    Col2 varchar(3);
    cOL3 date;
    )
```
then the column names in the database will be `col1`, `col2`, and `col3`.
