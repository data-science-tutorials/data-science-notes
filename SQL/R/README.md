# SQL Client - R

[TOC]


# Reading/Sources
- [SQL Tutorial](https://www.w3schools.com/sql/default.asp)


# Connecting to MariaDB from R
```R
install.packages("RMariaDB")
sql_con <- RMariaDB::dbConnect(drv = RMariaDB::MariaDB(),
                               dbname = "database_name",
                               username = "my_user_name",
                               password = "my_pass_word",
                               host = "localhost",
                               port = 3306)
```
