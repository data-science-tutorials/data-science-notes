# SQL Client - DBeaver

[TOC]


# Reading/Sources
- [SQL Tutorial](https://www.w3schools.com/sql/default.asp)
- [DBeaver](https://dbeaver.io/)


# [DBeaver](https://dbeaver.io/)
- Installation
```bash
sudo apt update
sudo apt -y  install openjdk-11-jdk openjdk-11-jre
sudo apt -y install default-jdk
wget -O - https://dbeaver.io/debs/dbeaver.gpg.key | sudo apt-key add -
echo "deb https://dbeaver.io/debs/dbeaver-ce /" | sudo tee /etc/apt/sources.list.d/dbeaver.list
sudo apt update
sudo apt -y  install dbeaver-ce
```
- Usage example. Connect to mariaDB (see file *SQL/SQL-server.md*)
	- Launch DBeaver
	- New connection
	- MariaDB
	- add connection details
	- Test connection > Download driver if necessary
	- Finish
- Now you can create DBs, tables, users and so on also from DBeaver (as long as you are connected with a user with enough privileges).
