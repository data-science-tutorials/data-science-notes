# MySQL Server

[TOC]

# Sources/Reading
- [MySQL](https://www.mysql.com/)


# MySQL
- Install `sudo apt update && apt install mysql-client mysql-server -y`
- Configuration
	- Open a terminal and launch `sudo mysql_secure_installation`
	- default root password is blank, just press enter
	- set up root password and follow instructions
	- to connect `sudo mysql`
- Create a user with admin rights that do not require sudo
```sql
sudo mysql
GRANT ALL ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
FLUSH PRIVILEGES;
```
- To connect to the server as root `su && mysql`
- Creating DBs and users
	- Conenct to the server as root
	- Create DB
	```sql
	CREATE DATABASE 'yourDB';
	SHOW DATABASES;
	```
	- Create user
	```sql
	CREATE USER 'user1'@localhost IDENTIFIED BY 'password1';
	SELECT User FROM mysql.user;
	```
	- Grant privileges
	```sql
	GRANT ALL PRIVILEGES ON yourDB.* TO 'user1'@localhost IDENTIFIED BY 'password1';
	FLUSH PRIVILEGES;
	SHOW GRANTS FOR 'user1'@localhost;
	```
	- Delete a user `DROP USER 'user1'@localhost;`
