# MariaDB SQL Server

[TOC]

# Sources/Reading
- [MariaDB](https://mariadb.com/)


# MariaDB
- Install `sudo apt update && apt install mariadb-client mariadb-server -y`
- Configuration
	- Open a terminal and launch `sudo mysql_secure_installation`
	- default root password is blank, just press enter
	- set up root password and follow instructions
	- to connect `sudo mariadb`
- Create a user with admin rights that do not require sudo and can log-in remotely
	```sql
	sudo mariadb
	GRANT ALL ON *.* TO 'admin'@'localhost' IDENTIFIED BY 'password' WITH GRANT OPTION;
	FLUSH PRIVILEGES;
	```
- To connect to the server as root `su && mariadb`
- Creating DBs and users
	- Connect to the server as root
	- Create DB
	```sql
	CREATE DATABASE [yourDB];
	SHOW DATABASES;
	```
	- Create user
	```
	CREATE USER 'user1'@localhost IDENTIFIED BY 'password1';
	SELECT User FROM mysql.user;
	```
	- Grant privileges
	```sql
	GRANT ALL PRIVILEGES ON yourDB.* TO 'user1'@localhost IDENTIFIED BY 'password1';
	FLUSH PRIVILEGES;
	SHOW GRANTS FOR 'user1'@localhost;
	```
- Delete a user `DROP USER 'user1'@localhost;`


# Allowing remote access
- Open the terminal and `mariadb -u root -p`
- To see the allowed hosts for the user *database_username*, run: `SELECT host FROM mysql.user WHERE user = "database_username";`
- Find the remote device's IP *ip.address.remote.device* and run: `GRANT ALL ON database_name.* to 'database_username'@'ip.address.remote.device' IDENTIFIED BY 'database_password';`
- Make sure the changes take effect: `FLUSH PRIVILEGES;`
- Double check: `SELECT host FROM mysql.user WHERE user = "database_username";`
