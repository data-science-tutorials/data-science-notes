# Cómo usar el eDNI en Linux

[TOC]

## Referencias
- https://atareao.es/como/dni-electronico-en-linux/
- https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1112
- https://firmaelectronica.gob.es/Home/Descargas.html
- https://valide.redsara.es/valide/inicio.html

## Requisitos

### Lector de tarjetas
- Comprar un lector de tarjetas
- Instalar herramientas necesarias
```
sudo apt update
sudo apt install pcsc-tools pcscd pinentry-gtk2 libcci
```

### Módulo criptográfico
- Descargar el [módulo criptográfico](https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1112)
- Instalar
```
sudo dpkg -i libpkcs11-dnie_1.6.6_amd64.deb
```
- Si se queja de dependencias faltantes, instalarlas y probar de nuevo
```
sudo apt purge libpkcs11-dnie
sudo apt install pinentry-gtk2
sudo apt install pcscd
sudo dpkg -i libpkcs11-dnie_1.6.6_amd64.deb
```
### AutoFirma
- Descargar [AutoFirma_Linux/AutoFirma_1_6_5.deb](https://firmaelectronica.gob.es/Home/Descargas.html)
- Instalar
```
sudo apt update
sudo dpkg -i AutoFirma_Linux/AutoFirma_1_6_5.deb
```
- Si se queja de dependencias faltantes, instalarlas y probar de nuevo
```
sudo apt purge AutoFirma
sudo apt install default-jdk libnss3-tools
sudo dpkg -i AutoFirma_Linux/AutoFirma_1_6_5.deb
```


## Firmar documentos digitalmente
- Comprobar que el ordenador reconoce el lector
    - Ejecutar `lsub` antes y después de conectar el lector. Si aparece una línea extra es que funciona.
- Comprobar que lee correctamente el eDNI
    - Ejectuar `pcsc_scan` y después conectar el lector con el DNI introducido, deberíamos ver información acerca del DNI electrónico español.
- Continuar y lanzar el programa `AutoFirma`, nos pedirá el PIN de nuestro eDNI (se puede conseguir en comisaria).
- Seguir las pantallas de la aplicación AutoFirma para realizar los trámites necesarios.
