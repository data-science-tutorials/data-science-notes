library(ggplot2)
library(shiny)
library(tidyverse)

server <- function(input, output) {

    output$scatterPlot <- renderPlot({

        df <- readr::read_csv(paste0("./data/", input$file))

        ggplot2::ggplot(df) +
            ggplot2::geom_point(aes(x, y), color = input$color)
    })
}

shinyServer(server)
