# Scatter Shiny App

[TOC]

# Details
- *Disclaimer*: This is not my work but I don't remember from where I got it.
- This is just a small example of a shiny app that displays a scatter plot of 4 different data set.

# How to run
- From this directory, launch an **R** session and execute the command `shiny::runApp()`
