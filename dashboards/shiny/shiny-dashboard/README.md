# ShinyDashboard Shiny App

[TOC]

# Details
- [ShinyDashboard](https://rstudio.github.io/shinydashboard/)
- This is a small example of a Shiny app built on top of Rstudios' shinydashboard library.
- The buttons on the left-hand side panel are only for aesthetic purposes and do not have any functionality.


# How to run
- Install `R` and the packages: `shiny`, `shinydashboard` and `DT`
- From this directory, launch an **R** session and execute the command `shiny::runApp()`
