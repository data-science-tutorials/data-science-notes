# Shiny

[TOC]

# Sources/Reading
- [Shiny](https://shiny.rstudio.com/)
- [ShinyDashboard](https://rstudio.github.io/shinydashboard/)
- [ShinyProxy](https://www.shinyproxy.io/)


# Installation
- Launch **R** and run the command `install.packages("shiny")`
- Useful additional libraries `install.packages(c("shinydashboard", "DT", "shinyjs"))`


# Examples
To run any example, launch an **R** session and run the following command `shiny::runApp("dashboards/shiny/[example-name]/")`.
