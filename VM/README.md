# Virtual Machines

[TOC]

# Reading/Sources
- [virt-manager](https://virt-manager.org/)


# Virt-Manager
- Install [virt-manager](https://virt-manager.org/)
	- `sudo apt install virt-manager -y`
- Because VMs access the OS kernel it needs admin privileges. It's better to run it always as root but if you get tired you can do the following:
	- `sudo groupadd libvirtd`
	- `sudo usermod -aG libvirtd [your-daily-user]`
	- log-out and in from your user session
- How to run a VM?
	- Download the .iso of the OS that you want to use
	- Create an instance from virt-manager
	- Select enough RAM memory, processors and disc space
	- Install the OS
