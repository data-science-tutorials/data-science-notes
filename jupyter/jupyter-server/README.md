# Jupyter server configturation

[TOC]

When you launch a Jupyter server session, the port, token, etc. vary each time. If you need to specify them, run:
```bash
jupyter notebook --generate-config
```

This generates a configuration file called jupyter_notebook_config.py placed at home/user_name/.jupyter/. All the lines are commented. Unless you uncomment a line, the option corresponding to the line is the default one. E.g.:
```
## The port the notebook server will listen on (env: JUPYTER_PORT).
#  Default: 8888
# c.NotebookApp.port = 8888
```
This means that the port is always 8888. If you want to fix the value of the port, uncomment the line c.NotebookApp.port... and write the port where you want your server. Typically, you may want to force the token. In such a case, go to:
```
#  Setting to an empty string disables authentication altogether, which is NOT
#  RECOMMENDED.
#  Default: '<generated>'
# c.NotebookApp.token = '<generated>'
```
which means that the default value is generated randomly when you launch the server, uncomment the last line, and replace "<generated>" by the value you prefer.

You can specify many other options in this configuration file by changing the values of other variables. The names are kind of intuitive. E.g., if you want to manipulate the directory where the Jupyter session is launched, modify c.NotebookApp.notebook_dir.
