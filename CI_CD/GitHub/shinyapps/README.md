# Deploy a Shinyapp with GitHub actions

[TOC]

GitHub actions allow you to automatise your deployments. In this document, we explain how to publish or update a Shiny app (https://shiny.rstudio.com/) to shinyapps (https://www.shinyapps.io/) with a GitHub action whenever you push your code to master. We follow mainly https://www.r-bloggers.com/2021/02/deploy-to-shinyapps-io-from-github-actions/.

Even if this is ad-hoc for the case considered here, automatic updates for apps written with a different technologie and/or hosted in other service should be similar. These are the steps to follow:

1. Create an R script to deploy your app.
2. Create a Docker image which runs that R script.
3. Create a GitHub action to build and run the Docker image whenever your push your code to master.

In the following we explain in more detail these steps.

# 1. Script to update your app

1. Get a token for https://www.shinyapps.io/:
    1. Log in.
    2. Click on your avatar (upper-right corner).
    3. Click on `Tokens`.
    4. Click on `Add token`.

2. Create a file called `.Renviron` in the root directory of the repository. It must have the following structure:
```R
SHINY_ACC_NAME= # username for https://www.shinyapps.io/
TOKEN= # token for https://www.shinyapps.io/ (see previous point)
SECRET= # secret part of the token for https://www.shinyapps.io/ (see previous point)
APP_NAME= # name of the app in https://www.shinyapps.io
```
with the proper values for all the variables. __NOTE__: Add this file to .gitignore.

3. Create an R script, let's call it `deploy.R`, with the following structure (we assume here we are using the typical `ui.R` and `server.R` struture for the Shiny app, as well as the dependencies of your app are in the directory `functions/`; if you app does not obey to this structure, modify the values for the argument `appFiles` for the function `deployApp()`):
```R
library(rsconnect)
# a function to stop the script when one of the variables cannot be found. and to strip quotation marks from the secrets when you supplied them. (maybe it is just easier to never use them)
error_on_missing_name <- function(name){
    var <- Sys.getenv(name, unset=NA)
    if(is.na(var)){
        stop(paste0("cannot find ",name, " !"),call. = FALSE)
    }
    gsub("\"", '',var)
}
# Authenticate
setAccountInfo(name = error_on_missing_name("SHINY_ACC_NAME"),
               token = error_on_missing_name("TOKEN"),
               secret = error_on_missing_name("SECRET"))
# Deploy the application.
deployApp(
    appFiles = c("ui.R",
                 "server.R",
                 "functions"), # here you should include all the files and dirs your app is using
    appName = error_on_missing_name("APP_NAME"),
    appTitle = "shinyapplication",
    forceUpdate = TRUE)
```

4. Test your script. If everything is ok, `source("deploy.R")` should update your app. _NOTE_: We are assuming the script is placed in the root directory. This is not necessary.

# 2. Deployment from Docker

Then, you must create a Docker image to run the previous script. It should have both `ui.R` and `server.R` (or, alternatively, `app.R`), as well as all the files your app depends on. Besides, the R version of the Docker image must have installed all the packages your app depends on. Finally, it should also contain the script to deploy the app. Here is a minimal version assuming:
1. You have used renv to keep track of your package versions.
2. You have developed your app with the structure `ui.R` and `server.R`.
3. All the dependencies of your app are in the directory `functions/`.

```docker
# Base image https://hub.docker.com/u/rocker/
FROM rocker/r-base:4.1.0

# install some dependencies
RUN apt-get update -qq && apt-get -y --no-install-recommends install libcurl4-openssl-dev libssl-dev

# create shinyusr directory and move there
WORKDIR /home/shinyusr

# install renv & restore packages
RUN Rscript -e 'install.packages("renv")'
COPY renv.lock .
RUN Rscript -e 'renv::restore()'

# copy Shiny scripts
COPY server.R server.R
COPY ui.R ui.R

# create functions dir
RUN mkdir functions/

# copy functions
COPY functions/* functions/

# copy script to deploy app
COPY deploy.R deploy.R

# run deploy.R
CMD ["Rscript", "deploy.R"]
```

Then, build your image (here we are assuming the file is called `Dockerfile`, which is not needed, and that it's placed in the root directory of your repo, which is necessary):

```bash
docker build -t deploy_covid_dashboard .
```

Assuming everything was ok, run the image using `.Renviron` as your environment file:

```bash
docker run --env-file .Renviron deploy_covid_dashboard
```

The app should be updated now.

# 3. Create a GitHub action

Th goal here is to deploy your app whenever you push your code to master. However, your deployment script (or Docker image) depends on some environment variables stored in `.Renviron`, which is git-ignored. We use here GitHub secrets:
1. Go to your GitHub account.
2. Open your repository.
3. Click on Settings.
4. Click on Secrets (left-hand side bar menu).
5. Click on New repository secret.
    * Name: Environment variable name (e.g. `SHINY_ACC_NAME`).
    * Value: Value of the variable.

Now, in your repository root directory, create a directory .github/workflows/ (this might also be done from the web interface). On it, create the following .yml file:

```yaml
# decriptive name
name: Run on push master
# configuration to trigger the workflow: it happens whenever we push to master
on:
branch
    push:
        branches: [ master ]
# here the fun starts
jobs:
# this workflow contains a single job called "build"
    build:
        # we use Ubuntu-20.04 (this is optional)
        runs-on: Ubuntu-20.04
        # steps
        steps:
            # I don't understand this
            - uses: actions/checkout@v2
            # build the docker image and give it the name main
            - run: docker build -t main .
            # run the docker image supply the secrets from the GitHub secrets store.
            - run: docker run -e SHINY_ACC_NAME=${{ secrets.SHINY_ACC_NAME }} -e TOKEN=${{secrets.TOKEN}} -e SECRET=${{secrets.SECRET}} -e APP_NAME=${{secrets.APP_NAME}} main
```

Now, whenever you push your code, the action will trigger and the app will be updated to shinyapps.

TO IMPROVE: This action does not use the cache whenever it builds the Docker image. It would be nice to upload the Docker image which updates the app to some container repository (e.g. GitHub repository: https://nikiforovall.github.io/docker/2020/09/19/publish-package-to-ghcr.html) and then modify the .yml file so that the cache is used (https://github.com/marketplace/actions/build-docker-images-using-cache).
