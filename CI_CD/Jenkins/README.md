# Jenkins

[TOC]

---
# Intro
Jenkins is one of the main devops tools https://www.jenkins.io/.


---
# Install on Debian-based distributions

Open your terminal and run:

```bash
wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > /etc/apt/sources.list.d/jenkins.list'
```
so that Jenkins is installable. Then:

```bash
sudo apt-get update
sudo apt-get install jenkins
```

Jenkins requires Java:

```bash
sudo apt install openjdk-11-jdk
```

To make sure that the installation of Java was ok, run:

```bash
java -version
```

The output should be something like this:

```bash
openjdk version "11.0.10" 2021-01-19
OpenJDK Runtime Environment (build 11.0.10+9-Ubuntu-0ubuntu1.20.04)
OpenJDK 64-Bit Server VM (build 11.0.10+9-Ubuntu-0ubuntu1.20.04, mixed mode, sharing)
```

To start Jenkins, run:

```bash
sudo systemctl start jenkins
```

To check Jenkins status, run:

```bash
sudo systemctl status jenkins
```

The output should be something like this:

```bash
● jenkins.service - LSB: Start Jenkins at boot time
     Loaded: loaded (/etc/init.d/jenkins; generated)
     Active: active (exited) since Thu 2021-04-22 23:58:13 CEST; 2min 47s ago
       Docs: man:systemd-sysv-generator(8)
      Tasks: 0 (limit: 9342)
     Memory: 0B
     CGroup: /system.slice/jenkins.service
```

# Getting started with Jenkins

Now Jenkins is running in your computer. To access to it, open your browser and navigate to http://localhost:8080.

You should see an interface which asks you for your Jenkins password. It is the alphanumerical code stored at /var/lib/jenkins/secrets/initialAdminPassword (you will probably need sudo rights to get it). Copy-paste this code and click on "Continue".

Now you can install the suggested plugins or customise your Jenkins.

Finally, you are asked to create your administration user. Introduce your credentials and you are good to go.
