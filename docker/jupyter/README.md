# Example of deploying a Jupyter server via Docker

[TOC]

# Reading
- https://nteract.gitbooks.io/hydrogen/content/docs/Usage/RemoteKernelConnection.html

# Launch the jupyter server
- Download the docker image `docker pull jupyter/minimal-notebook`
- Run the container `docker run --name jupyter-test -p 8888:8888 jupyter/minimal-notebook`
- Copy the URL and the token

# Connect to the server

## Web Browser
- Open your web browser
- Paste the URL
- Introduce the token

## IDEs
- Check the IDEs pages for more details.
