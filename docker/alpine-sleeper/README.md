# Alpine Sleeper Docker Image

[TOC]

# Details
- This is just a minimum example of a Dockerfile.
- It starts from the smallest operating system images (alpine).
- It waits 5 seconds before ending.


# Create and run the image
- To create the image, from this directory just run `docker build -t alpine-sleeper .`
- To run the freshly created image `docker run alpine-sleeper`
