# Python Hello World Docker Image

[TOC]

# Details
- This is just a minimum example of a Dockerfile.
- It starts from a R docker image.
- It run the R script *script.R*


# Create and run the image
- To create the image, from this directory just run `docker build -t r-hello-world .`
- To run the freshly created image `docker run r-hello-world`
