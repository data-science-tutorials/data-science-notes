# Example of deploying a MariaDB server via Docker

[TOC]

# Launch the [MariaDB](https://mariadb.org/) server
- Run a container of the image **mariadb** with name `[cont-name]`, on port `[port]` and with password `[password]` with the command:
 `docker run --name [cont-name] -p [port]:3306 -e MYSQL_ROOT_PASSWORD=[password] -d mariadb`.
- For example `docker run --name mariadb-test -p 3307:3306 -e MYSQL_ROOT_PASSWORD=test -d mariadb`


# Connect from a client (example from DBeaver)
- Start [DBeaver](https://dbeaver.io/)
- Start a new connection
- Select MariaDB (install drivers if asked)
- Host: `localhost`
- Port: `[port]`,i.e. , the one you specified on the docker run command
- Username: `root`
- Password: `[pasword]`
- Click on "Test Connection"
- Click on "Finish"

# Add some data (example from DBeaver)
- Go to the new added Connection an expand until postgres > Schemas > public > Tables
- Right-click on tables and "Import Data"
- Import [your favourite data](https://archive.ics.uci.edu/ml/datasets/Iris/) in CSV format

# Extra MariaDB Server Configuration
- See the [MariaDB](../../SQL/MariaDB/README.md) page.
