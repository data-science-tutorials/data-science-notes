# GitLab Container Registry

[TOC]

# Log-in with Docker to GitLab
- Generate a [personal access token](https://gitlab.com/-/profile/personal_access_tokens) with all the read permissions buy _only_ the write registry permission. Save the token value and the token name.
- Log into GitLab with Docker `docker login registry.gitlab.com`
    - You will be asked for your username and password (use the PAT that you generated on the previous step.)
- Credentials are stored _unencrypted_ on ~/.docker/config.json


# Build and upload images to GitLab Registry
- From the directory with the Dockerfile, run `docker build -t registry.gitlab.com/url/path/to/repo .`
- To upload it to GitLab, run `docker push registry.gitlab.com/url/path/to/repo`
