# Docker

[TOC]

---
# Reading/Sources
- [Official Web](https://www.docker.com/)
- [Docker Hub](https://hub.docker.com/)


---
# Installation
- Check the [official installation instructions](https://docs.docker.com/engine/install/debian/)
- But when adding the repository add it as a new file in `/etc/apt/sources.list.d/`
```bash
nano /etc/apt/sources.list.d/docker.list
deb [arch=amd64] https://download.docker.com/linux/debian buster stable
```
- Because docker access the OS kernel it needs admin privileges. If you don't want to go root every time you need to run it, you can do the following:
```bash
sudo groupadd docker
sudo usermod -aG docker [your-daily-user]
```
- log-out and in from your user session
- Docker is supposed to be safe and to isolate the containers that it uses. However, it has access to the kernel and a [user of a container can potentially access the host's system as root](https://stackoverflow.com/questions/57731428/how-do-i-prevent-root-access-to-my-docker-container/57732197#57732197).
- It's always a good idea to be careful with the docker images that you run, specially non-official ones.


## Docker on GNOME desktop
- GNOME shell extension `Docker Integration`

---
# Docker First Steps


## Reading/Sources
- [Tutorial for Beginners](https://docker-curriculum.com/)
- [Lab Exercises](https://kodekloud.com/p/docker-labs)
- Towards Data Science Articles
    - [Docker for Data Science](https://towardsdatascience.com/docker-for-data-science-9c0ce73e8263)
    - [Best Practices for Docker for Data Science](https://towardsdatascience.com/docker-best-practices-for-data-scientists-2ed7f6876dff)


## Basic commands
- start a container `docker run nginx`
- list running containers `docker ps`
- list all containers `docker ps -a`
- stop a container `docker stop [container-name-or-ID]`
- remove a container `docker rm [container-name-or-ID]`
- list images `docker images`
- remove image
	- first stop and remove all the containers depending on the image
	- `docker rmi nginx`
- Pull (downloads but doesn't run an instance of the image) `docker pull ubuntu`
- Pull a specific tag of an image `docker pull ubuntu:16.04`
- Rename a running container `docker rename [container-name-or-ID] [new-name]``
- Check docker networks `docker network`
- Inspect a container `docker inspect container-name-or-ID`


## Attach and detach
- To run an image but recover a working terminal (the container will run in the background) `docker run -d [container-name-or-ID]`
- To attach the terminal to a running container `docker run -a [container-name-or-ID]`
- Run a command on a running container `docker exec [container-name-or-ID] [command]`


## Work on a container
- If you haven't done it before, `docker pull ubuntu`
- `docker run -it ubuntu bash`
- now your are inside a bash terminal on a running ubuntu docker container
- In general, `docker run image_name` runs the built image and executes the **CMD** of your **Dockerfile** (if there's some).
- `docker run -it image_name bash`, ignores the command CMD and executes the bash.


## Create your own Docker images
- You will have to specify all the details in a file called *Dockerfile*. This file should be placed on a directory with all the necessary files for the image.
- To build the image, first go to this directory
```bash
cd docker/docker-examples/[example-name]
docker build -t [name for the image] .
```
- To run the freshly created image `docker run [name for the image]`


## Best practices
- **.dockerignore** file
