# Python Hello World Docker Image

[TOC]

# Details
- This is just a minimum example of a Dockerfile.
- It starts from a python docker image.
- It run the python script *script.py*


# Create and run the image
- To create the image, from this directory just run `docker build -t python-hello-world .`
- To run the freshly created image `docker run python-hello-world`
