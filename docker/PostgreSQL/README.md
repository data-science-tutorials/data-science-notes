# Example of deploying a PostgreSQL server via Docker

[TOC]

# Launch the [PostreSQL](https://www.postgresql.org/) server
- Run a container of the image **postgres** with name `[cont-name]`, on port `[port]` and with password `[password]` with the command:
 `docker run --name [cont-name] -p [port]:5432 -e POSTGRES_PASSWORD=[password] -d postgres`.
- For example `docker run --name psql-test -p 5433:5432 -e POSTGRES_PASSWORD=postgres -d postgres`


# Connect from a client (example from DBeaver)
- Start [DBeaver](https://dbeaver.io/)
- Start a new connection
- Select PostreSQL (install drivers if asked)
- Host: `localhost`
- Port: `[port]`,i.e. , the one you specified on the docker run command
- Database: `postgres`
- Username: `postgres`
- Password: `[pasword]`
- Click on "Test Connection"
- Click on "Finish"


# Add some data (example from DBeaver)
- Go to the new added Connection an expand until postgres > Schemas > public > Tables
- Right-click on tables and "Import Data"
- Import [your favourite data](https://archive.ics.uci.edu/ml/datasets/Iris/) in CSV format
