# Example of deploying a Grafana server via Docker

[TOC]

# Launch the [Grafana](https://grafana.com/) server
- Run a container of the image **grafana/grafana** with name `[cont-name]` on port `[port]` with the command: `docker run --name [cont-name] -d -p [port]:3000 grafana/grafana`.
- For example `docker run --name grafana-docker -d -p 3001:3000 grafana/grafana`
- Test it from your browser by going to `localhost:[port]`, e.g., `localhost:3001`. The default credentials are both `admin`.


# Add a Test Data Source
- On the left panel, click on the gear > Data Sources
- Add Data Source > TestData DB
- Save & Test


# Add a Dashboard for the Test Data Source
- On the left panel, click on the plus sign > Dashboard
- Click on "Add new panel"
- Select the "TestData DB"
- Click on "Apply"
- Save dashboard


# Add a Data Source from PostreSQL
- Follow the steps on `examples/PostreSQL` to deploy a SQL server with some data to be read by grafana.
- On the left panel, click on the gear > Data Sources
- Add Data Source > PostreSQL
- Host: `[host-ip]:[port]`, where `[host-ip]`is the IP of the docker container running the PostreSQL server (run `docker inspect [postgresql-docker-container-name]` to find it) and `[port]` is the port defined during the PostreSQL docker deployment
- Database `postgres`
- User: `postgres`
- Password: `[password]` (set up during the PostreSQL docker deployment)
- SSL mode: `disable`
- Save & Test
- *Bonus*: create a PostreSQL user just for grafana with read-only privileges.


# Add a Dashboard from PostreSQL
- On the left panel, click on the plus sign > Dashboard
- Click on "Add new panel"
- Select the "PostreSQL Data Source"
- Add data by selecting columns, panel visualisation and so on...
- Click on "Apply"
- Save dashboard
